package businessobject;

import dto.TestDataDTO;
import pageobject.*;

public class LoginationBO {

    private LoginPage loginPage;

    private HomePage homePage;

    public LoginationBO() {
        loginPage = new LoginPage();
        homePage = new HomePage();
    }

    public void logIn(TestDataDTO testDataDTO) {
        loginPage.typeEmailAndSubmit(testDataDTO.getEmail());
        loginPage.typePasswordAndSubmit(testDataDTO.getPassword());
    }

    public boolean areAccountOptionsPresent() {
        return homePage.areAccountOptionsPresent();
    }

    public void logOut() {
        homePage.logOut();
    }
}
