package dto;

import java.io.Serializable;
import java.util.Objects;

public class TestDataDTO implements Serializable {
    private static final long serialVersionUID = -3938922435114842114L;

    private int id;
    private String email;
    private String password;
    private String messageSubject;
    private boolean equal;

    public int getId() {
        return id;
    }

    public String getEmail() {
        return email;
    }

    public String getPassword() {
        return password;
    }

    public String getMessageSubject() {
        return messageSubject;
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public void setMessageSubject(String messageSubject) {
        this.messageSubject = messageSubject;
    }

    public void setEqual(boolean equal) {
        this.equal = equal;
    }

    public boolean isEqual() {
        return equal;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        TestDataDTO that = (TestDataDTO) o;
        return id == that.id &&
                equal == that.equal &&
                Objects.equals(email, that.email) &&
                Objects.equals(password, that.password) &&
                Objects.equals(messageSubject, that.messageSubject);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, email, password, messageSubject, equal);
    }

    @Override
    public String toString() {
        return "TestDataDTO{" +
                "id=" + id +
                ", email='" + email + '\'' +
                ", password='" + password + '\'' +
                ", messageSubject='" + messageSubject + '\'' +
                ", equal=" + equal +
                '}';
    }

    public static TestDataBuilder builder() {
        return new TestDataBuilder();
    }

    public static class TestDataBuilder {

        public TestDataDTO dataDTO = new TestDataDTO();

        public TestDataBuilder testDataId(int id) {
            dataDTO.id = id;
            return this;
        }

        public TestDataBuilder testDataEmail(String email) {
            dataDTO.email = email;
            return this;
        }

        public TestDataBuilder testDataPassword(String pass) {
            dataDTO.password = pass;
            return this;
        }

        public TestDataBuilder testDataMessageSubject(String messageSubject) {
            dataDTO.messageSubject = messageSubject;
            return this;
        }

        public TestDataBuilder testDataAssertion(boolean assertion) {
            dataDTO.equal = assertion;
            return this;
        }

        public TestDataDTO build() {
            return dataDTO;
        }
    }

}
