package userdata;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import dto.TestDataDTO;

import java.io.*;
import java.util.Arrays;
import java.util.List;

public class DataDAO {

    private Gson gson = new GsonBuilder().setPrettyPrinting().create();

    private String PATH_JSON_FILE = System.getProperty("user.dir") +
            File.separator + "src\\main\\resources\\testdata.json";

    void saveAll(List<TestDataDTO> testDTOList) throws IOException {
        Writer fileWriter = new FileWriter(PATH_JSON_FILE);
        fileWriter.write(gson.toJson(testDTOList));
        fileWriter.close();
    }

    @SuppressWarnings("unchecked")
    public List<TestDataDTO> getAll() throws IOException {
        FileReader fileReader = new FileReader(PATH_JSON_FILE);
        List<TestDataDTO> testDTOList = Arrays.asList(gson.fromJson(fileReader, TestDataDTO[].class));
        fileReader.close();
        return testDTOList;
    }
}
