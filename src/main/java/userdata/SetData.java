package userdata;

import dto.TestDataDTO;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class SetData {
    public static void main(String[] args) throws Exception {

        DataDAO dataDAO = new DataDAO();
        TestDataDTO data1 = TestDataDTO.builder()
                .testDataId(0)
                .testDataEmail("testepamivan@gmail.com")
                .testDataPassword("myprojectmailsende@1")
                .testDataMessageSubject("Test subject")
                .testDataAssertion(true)
                .build();
        TestDataDTO data2 = TestDataDTO.builder()
                .testDataId(1)
                .testDataEmail("testepampetro@gmail.com")
                .testDataPassword("myprojectmailsende@2")
                .testDataMessageSubject("Some other subject")
                .testDataAssertion(false)
                .build();
        TestDataDTO data3 = TestDataDTO.builder()
                .testDataId(2)
                .testDataEmail("test1pavlo@gmail.com")
                .testDataPassword("myprojectmailsende@3")
                .testDataMessageSubject("Test subject")
                .testDataAssertion(true)
                .build();
        TestDataDTO data4 = TestDataDTO.builder()
                .testDataId(3)
                .testDataEmail("dimamalash5@gmail.com")
                .testDataPassword("myprojectmailsende@4")
                .testDataMessageSubject("Yet another subject")
                .testDataAssertion(false)
                .build();
        TestDataDTO data5 = TestDataDTO.builder()
                .testDataId(4)
                .testDataEmail("gavruloepam@gmail.com")
                .testDataPassword("myprojectmailsende@5")
                .testDataMessageSubject("Test subject")
                .testDataAssertion(true)
                .build();

        List<TestDataDTO> testDTOList = new ArrayList<>();
        Collections.addAll(testDTOList, data1, data2, data3, data4, data5);
        dataDAO.saveAll(testDTOList);

        List<TestDataDTO> testDTOList1 = dataDAO.getAll();
        testDTOList1.forEach(System.out::println);
    }
}
