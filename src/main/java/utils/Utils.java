package utils;

import driver.DriverManager;
import dto.TestDataDTO;
import org.openqa.selenium.*;
import userdata.DataDAO;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import static logger.AllureLogger.*;

public class Utils {

    public static void goToPageURL(final String url) {
        logToAllureInfo("Going to URL: " + url);
        DriverManager.getDriver().get(url);
    }

    public static String getLocatorFromElement(WebElement element) {
        try {
            return element.toString()
                    .split("->")[1]
                    .replaceFirst("(?s)(.*)]", "$1" + "");
        } catch (ArrayIndexOutOfBoundsException e) {
            logToAllureError(String.valueOf(e.getCause()));
            return "parsing of locator from element failed !!";
        }
    }

    public static List<TestDataDTO> initializeTestData() {
        List<TestDataDTO> dataDTOArrayList = new ArrayList<>();
        try {
            logToAllureWarn("Extracting users data ");
            dataDTOArrayList = new DataDAO().getAll();
        } catch (IOException e) {
            logToAllureError(e.getMessage());
        }
        return dataDTOArrayList;
    }
}